﻿using System.Threading.Tasks;

namespace Ares.Web.App.Wrappers
{
    public interface IHttpClientWrapper
    {
        Task<TModel> GetJsonAsync<TModel>(string path);
        Task<TModel> PutJsonAsync<TModel>(string path, object content);
        Task<TModel> PostJsonAsync<TModel>(string path, object content);
        Task DeleteJsonAsync<TModel>(string path);
    }
}